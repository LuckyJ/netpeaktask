# Netpeak tweets task | Based on Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

##Task description
1. Поднять локально Laravel (версии 5.X).
2. Создать сервис-страницу с выводом сообщений из ленты твиттера одного из популярных сайтов (в реализации использовать апи твиттера или соответствующий пакет в Laravel).
3. Сообщения должны выводиться с автообновлением.

##Project upstart

###Download the project from bitbucket

`git clone https://LuckyJ@bitbucket.org/LuckyJ/netpeaktask.git`

`composer install`

`cp .env.example .env`

`php artisan key:generate`

###Create database user and grant privileges
`mysql -uroot -p -h ip_address`

`CREATE DATABASE twitterstream CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;`

`CREATE USER 'tweetmaster'@'172.17.0.1' IDENTIFIED BY 'm@sterpassw0rd';`

`GRANT ALL PRIVILEGES ON twitterstream. * TO 'tweetmaster'@'172.17.0.1';`

`FLUSH PRIVILEGES;`

###Configure database connection

Set connection parameters into .env file

###Run migration

`php artisan migrate`

###Go to the https://apps.twitter.com/app/new and create application

Grab the keys and access tokens. Add them to your .env file:

TWITTER_CONSUMER_KEY=123

TWITTER_CONSUMER_SECRET=123

TWITTER_ACCESS_TOKEN=123

TWITTER_ACCESS_TOKEN_SECRET=123

TWEETS_RESOURCE=resource

###Run connect to the twitter streaming API

`php artisan tweets:watch`

###Run queue
`php artisan queue:listen`

###Run web server

`php artisan serve`


Now go to the http://localhost:8000 and check result.

###Frontend

`npm install`

`gulp --production`

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).