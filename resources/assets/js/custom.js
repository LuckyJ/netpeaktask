$(document).ready( function () {

    const savedUri = "saved";
    const elemsPerPage = 5;
    const callInterval = 2500; //ms

    var baseUrl = window.location.protocol + "//" + window.location.host + "/";

    var id = $('.media').attr('data-id');
    var tweetlist = $('.tweet-list');

    /*
     * This function makes ajax call to the laravel for new tweets and,
     * if new tweets exists, replace current elements on new.
     *
     * @param {int} id
     * id - an id of latest tweet
     */
    function update(id) {
        $.ajax({
            type: "POST",
            async: true,
            data: {'id': id},
            url: baseUrl + 'api/v1/get-tweets',
            success: function (data) {
                setTimeout(function() {
                    tweetlist.prepend(data);
                    $('.tweet').slice(elemsPerPage, 999).remove();
                    id = $('.media').attr('data-id');
                    update(id);
                }, callInterval);
            }
        });
    }

    /*
     * Function update working only on / page.
     */
    if ($(location).attr('href').search(savedUri) == -1) {
        update(id);
    }
});
