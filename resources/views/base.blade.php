<!DOCTYPE html>
<html lang="en">
    <head>
        @include('components.header')
    </head>
    <body>
    <div class="flex-center position-ref full-height">
        @include('components.nav')
        @yield('content')
    </div>
    @include('components.footer')
    </body>
</html>