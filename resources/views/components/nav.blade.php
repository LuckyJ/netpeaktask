<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Tweets watcher</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="{{ url('/') }}">Latest 5 tweets <span class="sr-only">(current)</span></a>
                </li>
                <li>
                    <a href="{{ url('/saved') }}">Saved tweets <span class="sr-only">(current)</span></a>
                </li>
            </ul>
        </div>
    </div>
</nav>