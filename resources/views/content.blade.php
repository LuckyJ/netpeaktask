@extends('base')

@section('content')
    <div class="container">
    <div class="content">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="tweet-list">
                    {{--On saved page shows all saved tweets without autoloading.--}}
                    {{--On base page shows last 5 tweets with autoloading new.--}}
                @if (Request::is('saved'))
                        @include('tweet.list-all')
                    @else
                        @include('tweet.list')
                    @endif
                </div>

            </div>
        </div>
    </div>
    </div>
@stop