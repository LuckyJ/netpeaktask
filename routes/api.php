<?php

use Illuminate\Http\Request;
use App\Models\Tweet;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
 * Returns list-new-tweets blade view with last 5 tweets
 */
Route::post('/v1/get-tweets', function (Request $request) {
    $lastSiteTweet = Tweet::where('id', $request->id)->first();
    $tweets = Tweet::where('created_at', '>', $lastSiteTweet->created_at)->orderBy('created_at', 'desc')->take(5)->get();
    return view('tweet.list-new-tweets', ['tweets' => $tweets]);
});