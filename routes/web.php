<?php
/*
 * Returns content view with last 5 tweets
 */
Route::get('/', function () {
    $tweets = App\Models\Tweet::orderBy('created_at','desc')->take(5)->get();
    return view('content', ['tweets' => $tweets]);
});

/*
 * Returns all saved tweets with pagination
 */
Route::get('/saved', function () {
    $tweets = App\Models\Tweet::orderBy('created_at','desc')->paginate(5);
    return view('content', ['tweets' => $tweets]);
});
