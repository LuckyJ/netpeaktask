<?php

namespace App\Twitter;

use App\Jobs\TweetProcess;
use Illuminate\Foundation\Bus\DispatchesJobs;
use OauthPhirehose;

/*
 * OauthPhirehose developer marks class as @internal, but it test task,
 * so I leave it as is
 */
class TwitterStream extends OauthPhirehose
{
	use DispatchesJobs;

	public function enqueueStatus($status)
	{
		$this->dispatch(new TweetProcess($status));
	}
}
